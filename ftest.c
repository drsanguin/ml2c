/**
 *  ftest.c engendre par ml2C 
 */
#include "runtime.h"

/**
 *  de'claration de la fonction null___1
 *    vue comme la structure Fun  : MLfun_null___1
 */ 
int MAX_MLfun_null___1 = 1;

typedef struct MLfun_null___1 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue* ); 


 } MLfun_null___1; 

  MLvalue invoke_real(MLvalue* l___2) {

    { 
      MLvalue* T___3;
      { 
        MLvalue* T___4;
        MLvalue* T___5;
        T___4=l___2;
        T___5=MLnil;
        T___3=MLequal( (MLvalue* )T___4,(MLvalue* )T___5);
      }
      if (((MLvalue*)T___3)->value.asBool->val)
        { 
          MLvalue* T___6;
          T___6=MLtrue;
          return T___6;
        }
      else
        { 
          MLvalue* T___7;
          T___7=MLfalse;
          return T___7;
        }
    }
  }

  MLvalue* invoke_MLfun_null___1(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_null___1 		- 1)) {
      return invoke_real(MLparam);
    }
    else {
      MLfun_null___1 l = new MLfun_null___1(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_null___1() {super();}

  MLfun_null___1(int n) {super(n);}

}
// fin de la structure MLfun_null___1


/**
 *  de'claration de la fonction g___8
 *    vue comme la structure Fun  : MLfun_g___8
 */ 
int MAX_MLfun_g___8 = 1;

typedef struct MLfun_g___8 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue* ); 


 } MLfun_g___8; 

  MLvalue invoke_real(MLvalue* x___9) {

    { 
      MLvalue* T___10;
      MLvalue* T___11;
      T___10=x___9;
      T___11=MLnil;
      return MLequal( (MLvalue* )T___10,(MLvalue* )T___11);
    }
  }

  MLvalue* invoke_MLfun_g___8(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_g___8 		- 1)) {
      return invoke_real(MLparam);
    }
    else {
      MLfun_g___8 l = new MLfun_g___8(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_g___8() {super();}

  MLfun_g___8(int n) {super(n);}

}
// fin de la structure MLfun_g___8


/**
 *  de'claration de la fonction h___12
 *    vue comme la structure Fun  : MLfun_h___12
 */ 
int MAX_MLfun_h___12 = 2;

typedef struct MLfun_h___12 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue*, MLvalue* ); 


 } MLfun_h___12; 

  MLvalue invoke_real(MLvalue* x___13, MLvalue* y___14) {

    { 
      MLvalue* T___15;
      MLvalue* T___16;
      T___15=x___13;
      T___16=y___14;
      return new_list( (MLvalue* )T___15,(MLvalue* )T___16);
    }
  }

  MLvalue* invoke_MLfun_h___12(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_h___12 		- 1)) {
      return invoke_real(MLenv[0], MLparam);
    }
    else {
      MLfun_h___12 l = new MLfun_h___12(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_h___12() {super();}

  MLfun_h___12(int n) {super(n);}

}
// fin de la structure MLfun_h___12


/**
 *  de'claration de la fonction q___17
 *    vue comme la structure Fun  : MLfun_q___17
 */ 
int MAX_MLfun_q___17 = 1;

typedef struct MLfun_q___17 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue* ); 


 } MLfun_q___17; 

  MLvalue invoke_real(MLvalue* x___18) {

    { 
      MLvalue* T___19;
      MLvalue* T___22;
      { 
        MLvalue* T___20;
        MLvalue* T___21;
        T___20=MLHD;
        T___21=x___18;
        T___19=(((MLfun)T___20)->value.asFun)->invoke(T___21);
      }
      { 
        MLvalue* T___23;
        MLvalue* T___24;
        T___23=MLTL;
        T___24=x___18;
        T___22=(((MLfun)T___23)->value.asFun)->invoke(T___24);
      }
      return new_pair( (MLvalue* )T___19,(MLvalue* )T___22);
    }
  }

  MLvalue* invoke_MLfun_q___17(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_q___17 		- 1)) {
      return invoke_real(MLparam);
    }
    else {
      MLfun_q___17 l = new MLfun_q___17(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_q___17() {super();}

  MLfun_q___17(int n) {super(n);}

}
// fin de la structure MLfun_q___17


/**
 *  de'claration de la fonction v___25
 *    vue comme la structure Fun  : MLfun_v___25
 */ 
int MAX_MLfun_v___25 = 1;

typedef struct MLfun_v___25 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue* ); 


 } MLfun_v___25; 

  MLvalue invoke_real(MLvalue* x___26) {

    return MLnil;
  }

  MLvalue* invoke_MLfun_v___25(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_v___25 		- 1)) {
      return invoke_real(MLparam);
    }
    else {
      MLfun_v___25 l = new MLfun_v___25(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_v___25() {super();}

  MLfun_v___25(int n) {super(n);}

}
// fin de la structure MLfun_v___25


/**
 *  de'claration de la fonction w___27
 *    vue comme la structure Fun  : MLfun_w___27
 */ 
int MAX_MLfun_w___27 = 2;

typedef struct MLfun_w___27 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue*, MLvalue* ); 


 } MLfun_w___27; 

  MLvalue invoke_real(MLvalue* l___28, MLvalue* x___29) {

    { 
      MLvalue* T___30;
      { 
        MLvalue* T___31;
        MLvalue* T___32;
        T___31=l___28;
        T___32=MLnil;
        T___30=MLequal( (MLvalue* )T___31,(MLvalue* )T___32);
      }
      if (((MLvalue*)T___30)->value.asBool->val)
        { 
          MLvalue* T___33;
          T___33=MLnil;
          return T___33;
        }
      else
        { 
          MLvalue* T___34;
          { 
            MLvalue* T___35;
            MLvalue* T___36;
            T___35=x___29;
            T___36=MLnil;
            T___34=new_list( (MLvalue* )T___35,(MLvalue* )T___36);
          }
          return T___34;
        }
    }
  }

  MLvalue* invoke_MLfun_w___27(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_w___27 		- 1)) {
      return invoke_real(MLenv[0], MLparam);
    }
    else {
      MLfun_w___27 l = new MLfun_w___27(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_w___27() {super();}

  MLfun_w___27(int n) {super(n);}

}
// fin de la structure MLfun_w___27


/**
 *  de'claration de la fonction map___37
 *    vue comme la structure Fun  : MLfun_map___37
 */ 
int MAX_MLfun_map___37 = 2;

typedef struct MLfun_map___37 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue*, MLvalue* ); 


 } MLfun_map___37; 

  MLvalue invoke_real(MLvalue* f___38, MLvalue* l___39) {

    { 
      MLvalue* T___40;
      { 
        MLvalue* T___41;
        MLvalue* T___42;
        T___41=ftest.null___1;
        T___42=l___39;
        T___40=(((MLfun)T___41)->value.asFun)->invoke(T___42);
      }
      if (((MLvalue*)T___40)->value.asBool->val)
        { 
          MLvalue* T___43;
          T___43=MLnil;
          return T___43;
        }
      else
        { 
          MLvalue* T___44;
          { 
            MLvalue* T___45;
            MLvalue* T___50;
            { 
              MLvalue* T___46;
              MLvalue* T___47;
              T___46=f___38;
              { 
                MLvalue* T___48;
                MLvalue* T___49;
                T___48=MLHD;
                T___49=l___39;
                T___47=(((MLfun)T___48)->value.asFun)->invoke(T___49);
              }
              T___45=(((MLfun)T___46)->value.asFun)->invoke(T___47);
            }
            { 
              MLvalue* T___51;
              MLvalue* T___54;
              { 
                MLvalue* T___52;
                MLvalue* T___53;
                T___52=ftest.map___37;
                T___53=f___38;
                T___51=(((MLfun)T___52)->value.asFun)->invoke(T___53);
              }
              { 
                MLvalue* T___55;
                MLvalue* T___56;
                T___55=MLTL;
                T___56=l___39;
                T___54=(((MLfun)T___55)->value.asFun)->invoke(T___56);
              }
              T___50=(((MLfun)T___51)->value.asFun)->invoke(T___54);
            }
            T___44=new_list( (MLvalue* )T___45,(MLvalue* )T___50);
          }
          return T___44;
        }
    }
  }

  MLvalue* invoke_MLfun_map___37(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_map___37 		- 1)) {
      return invoke_real(MLenv[0], MLparam);
    }
    else {
      MLfun_map___37 l = new MLfun_map___37(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_map___37() {super();}

  MLfun_map___37(int n) {super(n);}

}
// fin de la structure MLfun_map___37


/**
 *  de'claration de la fonction iter___57
 *    vue comme la structure Fun  : MLfun_iter___57
 */ 
int MAX_MLfun_iter___57 = 2;

typedef struct MLfun_iter___57 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue*, MLvalue* ); 


 } MLfun_iter___57; 

  MLvalue invoke_real(MLvalue* f___58, MLvalue* l___59) {

    { 
      MLvalue* T___60;
      { 
        MLvalue* T___61;
        MLvalue* T___62;
        T___61=ftest.null___1;
        T___62=l___59;
        T___60=(((MLfun)T___61)->value.asFun)->invoke(T___62);
      }
      if (((MLvalue*)T___60)->value.asBool->val)
        { 
          MLvalue* T___63;
          T___63=MLnil;
          return T___63;
        }
      else
        { 
          MLvalue* T___64;
          { 
            MLvalue* T___65;
            MLvalue* T___66;
            T___65=ftest.iter___57;
            { 
              MLvalue* T___67;
              MLvalue* T___68;
              T___67=f___58;
              { 
                MLvalue* T___69;
                MLvalue* T___70;
                T___69=MLTL;
                T___70=l___59;
                T___68=(((MLfun)T___69)->value.asFun)->invoke(T___70);
              }
              T___66=(((MLfun)T___67)->value.asFun)->invoke(T___68);
            }
            T___64=(((MLfun)T___65)->value.asFun)->invoke(T___66);
          }
          return T___64;
        }
    }
  }

  MLvalue* invoke_MLfun_iter___57(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_iter___57 		- 1)) {
      return invoke_real(MLenv[0], MLparam);
    }
    else {
      MLfun_iter___57 l = new MLfun_iter___57(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_iter___57() {super();}

  MLfun_iter___57(int n) {super(n);}

}
// fin de la structure MLfun_iter___57


/**
 *  de'claration de la fonction inter___71
 *    vue comme la structure Fun  : MLfun_inter___71
 */ 
int MAX_MLfun_inter___71 = 2;

typedef struct MLfun_inter___71 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue*, MLvalue* ); 


 } MLfun_inter___71; 

  MLvalue invoke_real(MLvalue* a___72, MLvalue* b___73) {

    { 
      MLvalue* T___74;
      { 
        MLvalue* T___75;
        MLvalue* T___76;
        T___75=a___72;
        T___76=b___73;
        T___74=MLgtint( (MLvalue* )T___75,(MLvalue* )T___76);
      }
      if (((MLvalue*)T___74)->value.asBool->val)
        { 
          MLvalue* T___77;
          T___77=MLnil;
          return T___77;
        }
      else
        { 
          MLvalue* T___78;
          { 
            MLvalue* T___79;
            MLvalue* T___80;
            T___79=a___72;
            { 
              MLvalue* T___81;
              MLvalue* T___86;
              { 
                MLvalue* T___82;
                MLvalue* T___83;
                T___82=ftest.inter___71;
                { 
                  MLvalue* T___84;
                  MLvalue* T___85;
                  T___84=a___72;
                  T___85=new_int(1);
                  T___83=MLaddint( (MLvalue* )T___84,(MLvalue* )T___85);
                }
                T___81=(((MLfun)T___82)->value.asFun)->invoke(T___83);
              }
              T___86=b___73;
              T___80=(((MLfun)T___81)->value.asFun)->invoke(T___86);
            }
            T___78=new_list( (MLvalue* )T___79,(MLvalue* )T___80);
          }
          return T___78;
        }
    }
  }

  MLvalue* invoke_MLfun_inter___71(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_inter___71 		- 1)) {
      return invoke_real(MLenv[0], MLparam);
    }
    else {
      MLfun_inter___71 l = new MLfun_inter___71(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_inter___71() {super();}

  MLfun_inter___71(int n) {super(n);}

}
// fin de la structure MLfun_inter___71


/**
 *  de'claration de la fonction mult___87
 *    vue comme la structure Fun  : MLfun_mult___87
 */ 
int MAX_MLfun_mult___87 = 2;

typedef struct MLfun_mult___87 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue*, MLvalue* ); 


 } MLfun_mult___87; 

  MLvalue invoke_real(MLvalue* x___88, MLvalue* y___89) {

    { 
      MLvalue* T___90;
      MLvalue* T___91;
      T___90=x___88;
      T___91=y___89;
      return MLmulint( (MLvalue* )T___90,(MLvalue* )T___91);
    }
  }

  MLvalue* invoke_MLfun_mult___87(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_mult___87 		- 1)) {
      return invoke_real(MLenv[0], MLparam);
    }
    else {
      MLfun_mult___87 l = new MLfun_mult___87(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_mult___87() {super();}

  MLfun_mult___87(int n) {super(n);}

}
// fin de la structure MLfun_mult___87


/**
 *  de'claration de la fonction umap___126
 *    vue comme la structure Fun  : MLfun_umap___126
 */ 
int MAX_MLfun_umap___126 = 2;

typedef struct MLfun_umap___126 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue*, MLvalue* ); 


 } MLfun_umap___126; 

  MLvalue invoke_real(MLvalue* l___127, MLvalue* x___128) {

    { 
      MLvalue* T___129;
      { 
        MLvalue* T___130;
        MLvalue* T___131;
        T___130=ftest.null___1;
        T___131=l___127;
        T___129=(((MLfun)T___130)->value.asFun)->invoke(T___131);
      }
      if (((MLvalue*)T___129)->value.asBool->val)
        { 
          MLvalue* T___132;
          T___132=MLnil;
          return T___132;
        }
      else
        { 
          MLvalue* T___133;
          { 
            MLvalue* T___134;
            MLvalue* T___139;
            { 
              MLvalue* T___135;
              MLvalue* T___138;
              { 
                MLvalue* T___136;
                MLvalue* T___137;
                T___136=MLHD;
                T___137=l___127;
                T___135=(((MLfun)T___136)->value.asFun)->invoke(T___137);
              }
              T___138=x___128;
              T___134=(((MLfun)T___135)->value.asFun)->invoke(T___138);
            }
            { 
              MLvalue* T___140;
              MLvalue* T___145;
              { 
                MLvalue* T___141;
                MLvalue* T___142;
                T___141=ftest.umap___126;
                { 
                  MLvalue* T___143;
                  MLvalue* T___144;
                  T___143=MLTL;
                  T___144=l___127;
                  T___142=(((MLfun)T___143)->value.asFun)->invoke(T___144);
                }
                T___140=(((MLfun)T___141)->value.asFun)->invoke(T___142);
              }
              T___145=x___128;
              T___139=(((MLfun)T___140)->value.asFun)->invoke(T___145);
            }
            T___133=new_list( (MLvalue* )T___134,(MLvalue* )T___139);
          }
          return T___133;
        }
    }
  }

  MLvalue* invoke_MLfun_umap___126(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_umap___126 		- 1)) {
      return invoke_real(MLenv[0], MLparam);
    }
    else {
      MLfun_umap___126 l = new MLfun_umap___126(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_umap___126() {super();}

  MLfun_umap___126(int n) {super(n);}

}
// fin de la structure MLfun_umap___126
/**
 * 
 */
class ftest {

  MLvalue* null___1= new_MLfun_null___1(1);
  MLvalue* g___8= new_MLfun_g___8(1);
  MLvalue* h___12= new_MLfun_h___12(2);
  MLvalue* q___17= new_MLfun_q___17(1);
  MLvalue* v___25= new_MLfun_v___25(1);
  MLvalue* w___27= new_MLfun_w___27(2);
  MLvalue* map___37= new_MLfun_map___37(2);
  MLvalue* iter___57= new_MLfun_iter___57(2);
  MLvalue* inter___71= new_MLfun_inter___71(2);
  MLvalue* mult___87= new_MLfun_mult___87(2);
  MLvalue* i___92;
  MLvalue* l___98;
  MLvalue* fd___106;
  MLvalue* ig___108;
  MLvalue* bi___114;
  MLvalue* ik___118;
  MLvalue* b___122;
  MLvalue* umap___126= new_MLfun_umap___126(2);
  MLvalue* value___146;

int main(int argc, char** args) {
 initializer(); 

{ 
  MLvalue* T___93;
  MLvalue* T___96;
  { 
    MLvalue* T___94;
    MLvalue* T___95;
    T___94=ftest.inter___71;
    T___95=new_int(1);
    T___93=(((MLfun)T___94)->value.asFun)->invoke(T___95);
  }
  T___96=new_int(10);
  i___92=(((MLfun)T___93)->value.asFun)->invoke(T___96);
}
{ 
  MLvalue* bidon___97;
  bidon___97=MLlr;
  bidon___97=MLruntime.MLprint( (MLvalue* )i___92);
}
{ 
  MLvalue* T___99;
  MLvalue* T___104;
  { 
    MLvalue* T___100;
    MLvalue* T___101;
    T___100=ftest.map___37;
    { 
      MLvalue* T___102;
      MLvalue* T___103;
      T___102=ftest.mult___87;
      T___103=new_int(5);
      T___101=(((MLfun)T___102)->value.asFun)->invoke(T___103);
    }
    T___99=(((MLfun)T___100)->value.asFun)->invoke(T___101);
  }
  T___104=i___92;
  l___98=(((MLfun)T___99)->value.asFun)->invoke(T___104);
}
{ 
  MLvalue* bidon___105;
  bidon___105=MLlr;
  bidon___105=MLruntime.MLprint( (MLvalue* )l___98);
}
fd___106=ftest.map___37;
{ 
  MLvalue* bidon___107;
  bidon___107=MLlr;
  bidon___107=MLruntime.MLprint( (MLvalue* )fd___106);
}
{ 
  MLvalue* T___109;
  MLvalue* T___110;
  T___109=ftest.map___37;
  { 
    MLvalue* T___111;
    MLvalue* T___112;
    T___111=ftest.mult___87;
    T___112=new_int(5);
    T___110=(((MLfun)T___111)->value.asFun)->invoke(T___112);
  }
  ig___108=(((MLfun)T___109)->value.asFun)->invoke(T___110);
}
{ 
  MLvalue* bidon___113;
  bidon___113=MLlr;
  bidon___113=MLruntime.MLprint( (MLvalue* )ig___108);
}
{ 
  MLvalue* T___115;
  MLvalue* T___116;
  T___115=ig___108;
  T___116=l___98;
  bi___114=(((MLfun)T___115)->value.asFun)->invoke(T___116);
}
{ 
  MLvalue* bidon___117;
  bidon___117=MLlr;
  bidon___117=MLruntime.MLprint( (MLvalue* )bi___114);
}
{ 
  MLvalue* T___119;
  MLvalue* T___120;
  T___119=ftest.map___37;
  T___120=ftest.mult___87;
  ik___118=(((MLfun)T___119)->value.asFun)->invoke(T___120);
}
{ 
  MLvalue* bidon___121;
  bidon___121=MLlr;
  bidon___121=MLruntime.MLprint( (MLvalue* )ik___118);
}
{ 
  MLvalue* T___123;
  MLvalue* T___124;
  T___123=ik___118;
  T___124=i___92;
  b___122=(((MLfun)T___123)->value.asFun)->invoke(T___124);
}
{ 
  MLvalue* bidon___125;
  bidon___125=MLlr;
  bidon___125=MLruntime.MLprint( (MLvalue* )b___122);
}
{ 
  MLvalue* T___147;
  MLvalue* T___150;
  { 
    MLvalue* T___148;
    MLvalue* T___149;
    T___148=ftest.umap___126;
    T___149=b___122;
    T___147=(((MLfun)T___148)->value.asFun)->invoke(T___149);
  }
  T___150=new_int(10);
  value___146=(((MLfun)T___147)->value.asFun)->invoke(T___150);
}
{ 
  MLvalue* bidon___151;
  bidon___151=MLlr;
  bidon___151=MLruntime.MLprint( (MLvalue* )value___146);
}
}}

// fin du fichier ftest.c
