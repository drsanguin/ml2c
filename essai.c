/**
 *  essai.c engendre par ml2C 
 */
#include "runtime.h"

/**
 *  de'claration de la fonction succ___1
 *    vue comme la structure Fun  : MLfun_succ___1
 */ 
int MAX_MLfun_succ___1 = 1;

typedef struct MLfun_succ___1 {

MLfun *fun; 

MLvalue* invoke_real (MLvalue* ); 


 } MLfun_succ___1; 

  MLvalue invoke_real(MLvalue* x___2) {

    { 
      MLvalue* T___3;
      MLvalue* T___4;
      T___3=x___2;
      T___4=new_int(1);
      return MLaddint( (MLvalue* )T___3,(MLvalue* )T___4);
    }
  }

  MLvalue* invoke_MLfun_succ___1(MLvalue* function , MLvalue MLparam){
    if (((function->value.asFun)->fun)->MLcounter == (MAX_MLfun_succ___1 		- 1)) {
      return invoke_real(MLparam);
    }
    else {
      MLfun_succ___1 l = new MLfun_succ___1(MLcounter+1);l.MLaddenv(MLenv,MLparam); return l;
    }
  }

  MLfun_succ___1() {super();}

  MLfun_succ___1(int n) {super(n);}

}
// fin de la structure MLfun_succ___1
/**
 * 
 */
class essai {

  MLvalue* succ___1= new_MLfun_succ___1(1);

int main(int argc, char** args) {
 initializer(); 

}}

// fin du fichier essai.c
