#ifndef RUNTIME_H
#define RUNTIME_H


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int max = 20;

 struct MLunit;
 struct MLint;
 struct MLdouble;
 struct MLstring;
 struct MLbool;
 struct MLpair;
 struct MLlist;
 struct MLfun;

typedef enum MLkind  { unit=1 , integer=2,
		       real=3 ,stringz=4,
		       boolean =5,pair =6,
		       list  =7,function= 8,
			primitive	} MLkind;

typedef struct MLValue {
  MLkind kind_;
  union {
    struct MLunit *asUnit;
    struct MLint  *asInt;
    struct MLbool * asBoolean;
    struct MLdouble *asDouble;
    struct MLstring *asString;
    struct MLpair *asPair;
    struct MLlist *asList;
    struct MLfun* asFun;
    struct MLprimitive *asPrimitive;
  } value; 
  void (*print) (void*);
 } MLvalue;

/************ MLunit ********/

typedef struct MLunit {
	int val;        
	void(*print)(void);	
	int(*MLaccess)(struct MLunit*);                
} MLunit;

void MLunit_print() {
        printf("()");
}

int MLunit_MLaccess(MLunit *this) {                
        return this->val;
}

MLunit* New_MLunit() {
       MLunit *this = malloc(sizeof(MLunit));       
       this->print = MLunit_print;
       this->MLaccess = MLunit_MLaccess;
       this->val = 0;       
       return this;
}

//********************** [MLbool] ***********************
typedef enum bool { 
	false, 
	true 
} bool;

typedef struct MLbool {
	bool val;        
	void(*print)(struct MLbool*);	
	bool(*MLaccess)(struct MLbool*);                
} MLbool;

void MLbool_print(MLbool *this) {
	if (this->val) 		
	        printf("true");
	else
		printf("false");
}

bool MLbool_MLaccess(MLbool *this) {                
        return this->val;
}

MLbool* New_MLbool(bool initVal) {
       MLbool *this = malloc(sizeof(MLbool));       
       this->print = MLbool_print;
       this->MLaccess = MLbool_MLaccess;
       this->val = initVal;       
       return this;
}

//*********************** [MLint] ***********************
typedef struct MLint {
	int val;        
	void(*print)(struct MLint*);	
	int(*MLaccess)(struct MLint*);                
} MLint;

void MLint_print(MLint *this) {
        printf("%d", this->val);
}

int MLint_MLaccess(MLint *this) {                
        return this->val;
}

MLint* New_MLint(int initVal) {
       MLint *this = malloc(sizeof(MLint));       
       this->print = MLint_print;
       this->MLaccess = MLint_MLaccess;
       this->val = initVal;     
       return this;
}

//********************** [MLdouble] **********************
typedef struct MLdouble {
	double val;        
	void(*print)(struct MLdouble*);	
	double(*MLaccess)(struct MLdouble*);                
} MLdouble;

void MLdouble_print(MLdouble *this) {
        printf("%lf", this->val);
}

double MLdouble_MLaccess(MLdouble *this) {                
        return this->val;
}

MLdouble* New_MLdouble(double initVal) {
       MLdouble *this = malloc(sizeof(MLdouble));       
       this->print = MLdouble_print;
       this->MLaccess = MLdouble_MLaccess;
       this->val = initVal;       
       return this;
}

//********************** [MLstring] **********************
typedef struct MLstring {
	char *val;        
	void(*print)(struct MLstring*);	
	char*(*MLaccess)(struct MLstring*);                
} MLstring;

void MLstring_print(MLstring *this) {
        printf("%s", this->val);
}

char* MLstring_MLaccess(MLstring *this) {                
        return this->val;
}

MLstring* New_MLstring(char *initVal) {
       MLstring *this = malloc(sizeof(MLstring));       
       this->print = MLstring_print;
       this->MLaccess = MLstring_MLaccess;
       this->val = initVal;
       return this;
}


/****************************  MLList *******************/ 

typedef struct MLlist {
  MLvalue* head;
  MLvalue  *next;   
  
  MLvalue* (*hd)(struct MLlist*);
  struct MLvalue*  (*tail) (struct MLlist*);
  void (*print)   (void*);
} MLlist;


MLvalue* get_tail (MLlist* list) {
  if (list != NULL)
    return list->next;
  else
    return NULL; 
}


/* To be changed*/

 MLvalue* access_value ( MLlist* list) {
  if(list!=NULL)
    return list->head;  
  return NULL;
  }

void print_list (MLlist *list) {
  if (list == NULL )
    return;
  else {
    printf ("");
    MLvalue* value = list->head;
    value->print (value);
    if (list->next != NULL)
      (list->next)->print (list->next);
   }
} 

/* To be changed */
MLlist* initialize_list (MLvalue* val, MLvalue* next) {
  if (next!=NULL)
    { 
      if(next->kind_ == list)
	{
	  // Type control over here; Lists contain only member of the same type
          MLvalue* header = next->value.asList->head;
          if(header !=NULL && val != NULL)
          if (header->kind_ != val->kind_ )
	    return NULL;
          }
      else
	return NULL;
      }
  MLlist* list = (MLlist*)malloc (sizeof(MLlist));
  list->head = val; 
  list->next = next;
  list->hd   =  &access_value;
  list->tail =  &get_tail; 
  list->print = &print_list;
  return list;
}

/****** MLPair   ******/

typedef struct MLpair {
  MLvalue* first;
  MLvalue* second;
  void (*print) ( void*);	
} MLpair;

MLvalue* get_first (MLvalue *paire ) {
 if(paire==NULL)
    return NULL;
 if (paire->kind_ != pair)
    return NULL;
 else
   return paire->value.asPair->first; 
 }

MLvalue* get_second (MLvalue *paire ) {
 if(paire==NULL)
    return NULL;
 if (paire->kind_ != pair)
    return NULL;
 else
   return paire->value.asPair->second; 
 }

void print_pair (MLpair *pair ) {
  if(pair == NULL)
    return;
  else 
    {
      printf ("(");
      MLvalue* premier=  pair->first;
      premier->print(premier);
      printf (" , ");
      MLvalue* deuxieme =  pair->second;
      deuxieme->print (deuxieme);
      printf (" )");  
 }
}


MLpair* initilize_pair (MLvalue* value1, MLvalue* value2 ) {
  MLpair* pair = (MLpair*) malloc (sizeof(MLpair));
  pair->first = value1;
  pair->second= value2;
  pair -> print = &print_pair;
  return pair;
 }


/*****  MLfun ******/

 typedef struct MLfun {
   int MLcounter;
   MLvalue* env;
   void (*print) (void*);
   void (*MLaddenv) (void *, MLvalue[], MLvalue *);
   MLvalue* (*invoke) (MLvalue* x);
   MLvalue* (*invoke_real) (MLvalue* x);  /* Can be omitted as each funvtion defines it's invoke_real method differently */
  } MLfun;


 typedef struct MLprimitive {
  MLfun* fun;
  char* name; 
 } MLprimitive;

  MLvalue* initialise_primitive (char *nom) {
  MLvalue *v = (MLvalue*)malloc(sizeof (MLvalue));
  v->kind_ = primitive; 
  MLprimitive* prim = (MLprimitive*)malloc(sizeof (MLprimitive)); 	
  prim->name = nom;
  prim->fun = (MLfun *)malloc (sizeof(MLfun));
  if (strcmp(nom,"hd")==0)
  {	
        prim->fun->invoke = &access_value; 
   }
   else if (strcmp(nom,"tl")==0) {
	prim->fun->invoke = &get_tail;
   }
   else if (strcmp(nom,"fst")==0) {
       prim->fun->invoke = &get_first; 
   }
   else if (strcmp(nom,"snd")==0) {
       prim->fun->invoke = &get_second;
   }
    else 
         return NULL;
  v->value.asPrimitive = prim;	
  return v;
  }

void addenv (MLfun *fun, MLvalue* envi, MLvalue *a) {
int i = 0;
if ( fun == NULL)
 return;

for (; i < fun->MLcounter; i++) {
    fun->env[i]= envi [i];
 }
  fun->env[i] = *a;
}  

void print_fun (MLfun* fun) {
  if (fun!= NULL)
    return;
  printf("<fun> [");
  int i = 0;
  MLvalue*  envs = fun->env;
  MLvalue* environ;
  for (; i < fun->MLcounter; i++ ) {
    environ =&envs[i];
    environ->print ( environ);
  }
  printf ("]");
}
 
MLfun* initialise_fun () {
  MLfun  *fun = (MLfun*)malloc (sizeof (MLfun));
  fun->MLcounter =0;
  fun->print = &print_fun;
  fun->MLaddenv = &addenv;
  return fun;
}

MLfun* initialize_fun (int n) {
  MLfun * fun = initialise_fun();
  MLvalue* values= (MLvalue*) malloc (n * sizeof ( MLvalue));
  fun->env= values;
  return fun; 
 }

MLvalue* new_unit (){
  MLunit * uni = New_MLunit();
  MLvalue* v= (MLvalue*)malloc(sizeof(MLvalue));
  v->kind_ = unit;
  v->value.asUnit = uni;
  v->print = uni->print;
  return v;
}

MLvalue* new_bool (bool val) {
  MLbool* asBool = New_MLbool(val);
  MLvalue* v= (MLvalue*)malloc(sizeof(MLvalue));
  v->kind_ = boolean;
  v->value.asBoolean = asBool;
  v->print = asBool->print;
  return v;
 }

MLvalue* new_int (int val) {
  MLint *intVal= New_MLint(val);
  MLvalue* v= (MLvalue*)malloc(sizeof(MLvalue));
  v->kind_ = integer;
  v->value.asInt = intVal;
  v->print = intVal->print;
  return v;
}

MLvalue* new_double ( double val ) {
  MLint *floatVal= New_MLdouble(val);
  MLvalue* v= (MLvalue*)malloc(sizeof(MLvalue));
  v->kind_ = real;
  v->value.asInt = floatVal;
  v->print = floatVal->print;
  return v;
}

MLvalue* new_string (char *chaine) {
  MLstring* string = New_MLstring (chaine);
  MLvalue* v= (MLvalue*)malloc(sizeof(MLvalue));
  v->kind_ = stringz;
  v->value.asString = string;
  v->print = string->print;
  return v;
 }

MLvalue* new_pair (MLvalue* v0 , MLvalue* v1)  {
  MLpair *couple =initilize_pair (v0,v1);
  MLvalue* v= (MLvalue*)malloc(sizeof(MLvalue));
  v->kind_ = pair;
  v->value.asPair = couple;
  v->print = couple->print;
  return v;
 }

MLvalue* new_list (MLvalue* v0, MLvalue* v1)  {
  MLlist* liste = initialize_list (v0,v1);
  MLvalue* v= (MLvalue*)malloc(sizeof(MLvalue));
  v->kind_ = list;
  v->value.asPair = liste;
  v->print = liste->print;
  return v;
 }

MLvalue* new_fun () {
  // TODO
  return NULL;
 }

// acces aux champs des paires
extern MLvalue* MLfst;
extern MLvalue* MLsnd;

#define MLFST(pair) MLfst->fun->invoke(pair);
#define MLSND(pair) MLsnd->fun->invoke(pair);

// acces aux champs des listes
extern MLvalue* MLhd;
extern MLvalue* MLtl;

#define MLHD(list) MLhd->fun->invoke(list);
#define MLTL(list) MLtl->fun->invoke(list);

// la fonction d'affichage
MLvalue* MLprint(MLvalue* x)
{
	x->print(x);
	printf("\n");	
	return new_unit();
}

MLint* addInt (MLint*, MLint*); 
MLint* subInt (MLint*, MLint*);
MLint* multInt (MLint*, MLint*);
MLint* divInt (MLint*, MLint*);
MLbool* equals (MLvalue* , MLvalue*);
MLbool* MLltint (MLint* , MLint *);
MLbool* MLletint (MLint*, MLint *);
MLbool* MLgtint (MLint*, MLint*);
MLbool* MLgeint (MLint* , MLint*); 
MLstring* MLconcat (MLstring* , MLstring*);
extern void initializer (); 
extern MLvalue *MLlr;
extern MLvalue *MLtrue, *MLfalse;
extern MLvalue *MLnil;

#endif 




